# Carstats

### What is Carstats?
Carstats is my implementation of a OBD2 data collection system.


### Why Are you building something (probably) already made?

 - Python
 - New package to try out
 - I have time
 - It's not node.js

## How do I use `Carstats`?
You ask politely. [Full instructions are on the Wiki.](https://gitlab.com/n0ric/carstats/wikis/home)